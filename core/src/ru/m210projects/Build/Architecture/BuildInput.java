// This file is part of BuildGDX.
// Copyright (C) 2017-2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BuildGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BuildGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BuildGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Build.Architecture;

import com.badlogic.gdx.Input;

import static ru.m210projects.Build.Engine.xdim;
import static ru.m210projects.Build.Engine.ydim;

public interface BuildInput extends Input {

	void init(BuildFrame frame);

	void dispose();

	void update();

	/**
	 * Center mouse or reset mouse coordinate for the next event.
	 * <p>
	 * This method was {@link ru.m210projects.Build.Pattern.BuildControls#resetMousePos}. We make it an instance method
	 * of BuildInput because it is sometimes (but not always) used for camera control, but some backends should not use
	 * manual re-centering.
	 * <p>
	 * This method may be called for two different purposes:
	 * <ul>
	 *     <li>When in menu, move the mouse cursor to the center of the screen</li>
	 *     <li>When doing camera control (walking in 3D), reset the mouse coordinate to the center of the window so that
	 *     when the next mouse cursor event comes, we compute the deltaX and deltaY relative to the center of the
	 *     window.</li>
	 * </ul>
	 * However, there are many call sites that call this method.  Some for one purpose, some for the other, and some
	 * conditionally does one of them.  We never know which the caller wants.
	 * <p>
	 * The first use case is OK.  However, in the second use case, GLFW explicitly recommends not to use
	 * glfwSetCursorPos for camera control, because when GLFW_CURSOR_DISABLED is set, GLFW automatically centers the
	 * mouse and put the mouse cursor in an unbounded coordinate system.  The LWJGL3 backend uses GLFW for input, but
	 * other backends still require manual re-centering cursor position.
	 * <p>
	 * For this reason, we extract this operation as a virtual method so that the LWJGL3 backend can skip manual
	 * re-centering while still performing cursor centering. Other backends can still behave in the traditional way.
	 * <p>
	 * See: https://www.glfw.org/docs/latest/group__input.html#ga04b03af936d906ca123c8f4ee08b39e7
	 */
	default void resetMousePos() {
		setCursorPosition(xdim / 2, ydim / 2);
	}

	void processEvents();

	void processMessages();

	boolean cursorHandler();

	int getDWheel();

}
